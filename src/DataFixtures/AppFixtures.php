<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\User;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            //category
            $category = new Category();
            $category->setName('category' . $i);
            $manager->persist($category);

            //user
            $user = new User();
            $user->setUsername('name' . $i);
            $user->setPassword("password");
            $manager->persist($user);

            //post
            $post = new Post();
            $post->setTitle('title' . $i);
            $post->setImage('C:\Users\praksa.1\Desktop\0');
            $post->setCategory($category);
            $post->setSlug('slug' . $i);
            $post->setDescription('desc' . $i);
            $post->setAuthor($user);
            $post->setCreatedAt(new \DateTime());
            $post->setUpdatedAt(new \DateTime());
            $manager->persist($post);
        }


        $manager->flush();
    }
}